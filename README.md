# Svelte JS #

Svelte es una alternativa a Angular, Vue JS & React JS.

### ¿Qué necesitas saber? ###

* Javascript
* CSS
* HTML
* Node JS (NPM)

### Instalar Svelte ###
```
$ npm install -g degit
$ npx degit sveltejs/template proyecto
```

#### degit ###

https://github.com/Rich-Harris/degit


### Crear proyecto con Svelte ###
```
$ npx degit sveltejs/template auditor
$ cd auditor

# Con Typescript
$ node scripts/setupTypeScript.js

$ npm install
$ npm run dev
```
Abrir el navegador http://localhost:5000

### Enlaces ###

* [https://svelte.dev/](https://svelte.dev/)
* [degit](https://github.com/Rich-Harris/degit)
* [El fácil camino a Svelte](https://svelte.dev/blog/the-easiest-way-to-get-started)
* [Empezar con Svelte](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started)
