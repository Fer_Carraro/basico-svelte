import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: ' al mundo Svelte'
	}
});

export default app;